/*
 This program toggles an LED based on information it recieves from the serial line
 
 Send it a "toggle" to toggle the LED
 send it a "status" to get the status of the LED
 */

// Our constants
int led = 13;
int baud = 9600;
String LEDStatus = "off";
String commandString = "";
String commandToggle = "toggle";
String commandStatus = "status";

void setup(){
  // Init LED
  pinMode(led, OUTPUT);
  // Init Serial
  Serial.begin(baud);
  Serial.println("Ready");
  // Reserve 200 bytes for commandString
  commandString.reserve(200);
}

void loop(){

}

void ParseCommand(String command){
  if(command == commandToggle){
    if(ToggleLED()){
      Serial.println("Toggled LED");
    } 
    else {
      Serial.println("Error Toggling LED");
    }
  }
  else if (command == commandStatus){
    GetLEDStatus();
    Serial.println("LED Status: " + LEDStatus);
  }
  else {
    Serial.println("Unrecognized Command");
  }
}
boolean GetLEDStatus(){
  if(LEDStatus == "on"){
    return true;
  }
  else {
    return false;
  }
}

boolean ToggleLED(){
  if(GetLEDStatus()){
    LEDStatus = "off";
    digitalWrite(led, LOW);
    return true;
  }
  else {
    LEDStatus = "on";
    digitalWrite(led, HIGH);
    return true;
  }
  return false; 
}

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read(); 

    // newline signifies the end of the command. Parse it.
    if (inChar == '\n') {
      ParseCommand(commandString);
      commandString = "";
    } else {
      commandString += inChar;
    }
  }
}




