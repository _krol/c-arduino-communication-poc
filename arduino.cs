﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TogglePOC
{
    class arduino
    {
        public int baud = 9600;
        public String port = "COM3";
        private static System.IO.Ports.SerialPort serialPort1;

        public arduino()
        {
            // constructor
            System.ComponentModel.IContainer components = new System.ComponentModel.Container();
            serialPort1 = new System.IO.Ports.SerialPort(components);
            serialPort1.PortName = port;
            serialPort1.BaudRate = baud;
        }

        public string getStatus()
        {
            // get the LED status from the arduino
            try
            {
                serialPort1.Open();
                serialPort1.Write("status" + "\n");
                String status = serialPort1.ReadLine();
                serialPort1.Close();
                return status;
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine("Error getting status " + ex);
            }

            return "error";
        }

        public bool toggleLED()
        {
            try
            {
                serialPort1.Open();
                serialPort1.Write("toggle" + "\n");
                String status = serialPort1.ReadLine();
                serialPort1.Close();
                return true;
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine("Error toggling LED " + ex);
            }
            return false;
        }
    }
}
