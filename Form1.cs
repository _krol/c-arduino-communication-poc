﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TogglePOC
{
    public partial class Form1 : Form
    {
        arduino myArduino = new arduino(); // our instance of arduino

        public Form1()
        {
            InitializeComponent();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            LEDStatusLabel.Text = myArduino.getStatus();
        }

        private void LEDToggleButton_Click(object sender, EventArgs e)
        {
            if (myArduino.toggleLED())
            {
                
            }
            LEDStatusLabel.Text = myArduino.getStatus();
        }
    }
}
