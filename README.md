# Proof of concept for USB Communication between a C# program and an Arduino #

## Abstract ##
The final product shall be the ability to turn a LED on an Arduino on or off via a desktop program written with C# .net.

## Defining Commands ##
I want a physical indication that the system is working. To accomplish this, I will be toggling an LED.

The Arduino program must be able to parse the following commands:

* toggle
* status


## Defining Constants ##
For the Proof of Concept, I am using the following constants:

~~~~
int led  = 13;   // Pin number where we hook up our LED
int baud = 9600; // Serial Baud rate
String LEDStatus = "off"; // status indicator
String commandString = "";  // Storage for our input strings 
String commandToggle = "toggle"; // Comparison string
String commandStatus = "status"; // Comparison string
~~~~

## Arduino Sketch ##
~~~~
/*
 This program toggles an LED based on information it recieves from the serial line

 Send it a "toggle\n" to toggle the LED
 Send it a "status\n" to get the status of the LED
 */

// Our constants
int led  = 13;   // Pin number where we hook up our LED
int baud = 9600; // Serial Baud rate
String LEDStatus = "off"; // status indicator
String commandString = "";  // Storage for our input strings 
String commandToggle = "toggle"; // Comparison string
String commandStatus = "status"; // Comparison string

// Setup function - required by default
void setup(){
  // Init LED
  pinMode(led, OUTPUT);
  // Init Serial
  Serial.begin(baud);
  Serial.println("Ready");
  // Reserve 200 bytes for commandString
  commandString.reserve(200);
}

/* Arduino main, but we rely on serialevent for this program */ 
void loop(){

}

void ParseCommand(String command){
  if(command == commandToggle){
    if(ToggleLED()){
      Serial.println("Toggled LED");
    } 
    else {
      Serial.println("Error Toggling LED");
    }
  }
  else if (command == commandStatus){
    GetLEDStatus();
    Serial.println("LED Status: " + LEDStatus);
  }
  else {
    Serial.println("Unrecognized Command");
  }
}

boolean GetLEDStatus(){
  if(LEDStatus == "on"){
    return true;
  }
  else {
    return false;
  }
}

boolean ToggleLED(){
  if(GetLEDStatus()){
    LEDStatus = "off";
    digitalWrite(led, LOW);
    return true;
  }
  else {
    LEDStatus = "on";
    digitalWrite(led, HIGH);
    return true;
  }
  return false; 
}

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read(); 

    // newline signifies the end of the command. Parse it.
    if (inChar == '\n') {
      ParseCommand(commandString);
      commandString = "";
    } else {
      commandString += inChar;
    }
  }
}
~~~~

## The C# code ##
~~~~
class arduino
    {
        public int baud = 9600;
        public String port = "COM3"; // Change this for your setup
        private static System.IO.Ports.SerialPort serialPort1;

        public arduino()
        {
            // constructor
            System.ComponentModel.IContainer components = new System.ComponentModel.Container();
            serialPort1 = new System.IO.Ports.SerialPort(components);
            serialPort1.PortName = port;
            serialPort1.BaudRate = baud;
        }

        public string getStatus()
        {
            // get the LED status from the arduino
            try
            {
                serialPort1.Open();
                serialPort1.Write("status" + "\n");
                String status = serialPort1.ReadLine();
                serialPort1.Close();
                return status;
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine("Error getting status " + ex);
            }

            return "error";
        }

        public bool toggleLED()
        {
            try
            {
                serialPort1.Open();
                serialPort1.Write("toggle" + "\n");
                String status = serialPort1.ReadLine();
                serialPort1.Close();
                return true;
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine("Error toggling LED " + ex);
            }
            return false;
        }
    }
~~~~

## Resources ##
[jtoee blog post (now defunct)](http://jtoee.com/2009/02/talking-to-an-arduino-from-net-c/)

See http://cynikre.com/blog/?p=486 for original blog post