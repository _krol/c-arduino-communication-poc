﻿namespace TogglePOC
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.LEDStatusLabel = new System.Windows.Forms.Label();
            this.LEDToggleButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Currently the LED is:";
            // 
            // LEDStatusLabel
            // 
            this.LEDStatusLabel.AutoSize = true;
            this.LEDStatusLabel.Location = new System.Drawing.Point(168, 61);
            this.LEDStatusLabel.Name = "LEDStatusLabel";
            this.LEDStatusLabel.Size = new System.Drawing.Size(35, 13);
            this.LEDStatusLabel.TabIndex = 1;
            this.LEDStatusLabel.Text = "label2";
            // 
            // LEDToggleButton
            // 
            this.LEDToggleButton.Location = new System.Drawing.Point(107, 148);
            this.LEDToggleButton.Name = "LEDToggleButton";
            this.LEDToggleButton.Size = new System.Drawing.Size(75, 23);
            this.LEDToggleButton.TabIndex = 2;
            this.LEDToggleButton.Text = "ToggleLED";
            this.LEDToggleButton.UseVisualStyleBackColor = true;
            this.LEDToggleButton.Click += new System.EventHandler(this.LEDToggleButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 222);
            this.Controls.Add(this.LEDToggleButton);
            this.Controls.Add(this.LEDStatusLabel);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Toggle LED";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LEDStatusLabel;
        private System.Windows.Forms.Button LEDToggleButton;
    }
}

